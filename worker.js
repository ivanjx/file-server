// Importing log module.
const log = require('./helpers/log').log;

// Importing config.
const Config = require('./config');

// Setting up express.
const express = require('express');
const app = express();

// Logging route.
app.use(function(req, res, next) {
    let address = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    log(address + ' ' + req.method + ': ' + req.url);
    next();
});

// File route.
const fileRoute = require('./routes/file')();
app.get('/file/list', fileRoute.list);
app.get('/file/size', fileRoute.size);
app.get('/file/download', fileRoute.download);

// Default route.
app.get('/', function(req, res) {
    res.json({ success: true, message: 'Hello world!' });
    return;
});

// Setting up http server.
const http = require('http');
const httpServer = http.createServer(app);

// Starting http server.
httpServer.listen(Config.serverConfig.port, function(err) {
    if(err) {
        log('Unable to start http server: ' + err.message);
    }
})