// Loading logging helper.
const log = require('./helpers/log').log;

// Loading cluster.
const cluster = require('cluster');

if(cluster.isMaster) {
    // Run as master.
    // Getting core cpu count.
    let cpuCount = require('os').cpus().length;

    // Handling cluster events.
    cluster.on('online', function(worker) {
        log({ pid: 'master', message: 'A worker started with PID: ' + worker.process.pid });
    });

    cluster.on('exit', function(worker, code, signal) {
        log({ pid: 'master', message: 'A worker was stopped: ' + worker.process.pid });
        
        // Starting new worker.
        cluster.fork();
    });

    cluster.on('message', function(worker, message, handle) {
        log(message);
    });

    // Starting workers.
    log({ pid: 'master', message: 'Starting workers...' });

    for(let i = 0; i < cpuCount; ++i) {
        cluster.fork();
    }

    log({ pid: 'master', message: 'Started workers count: ' + cpuCount.toString() });

    // Starting routine.
    //const routine = require('./helpers/routine');
    //routine.start();
} else {
    // Run as worker.
    require('./worker');
}