// File route.
module.exports = function() {
    const fs = require('fs');
    const path = require('path');
    const User = require('../helpers/user');
    const log = require('../helpers/log').log;

    let module = {};

    // GET
    // Query: username, password, path
    module.list = function(req, res) {
        // Validating.
        if(!req.query.username) {
            res.status(500);
            res.json({ success: false, message: 'Insufficient parameter: username' });
            return;
        }
        
        if(!req.query.password) {
            res.status(500);
            res.json({ success: false, message: 'Insufficient parameter: password' });
            return;
        }

        if(!req.query.path) {
            res.status(500);
            res.json({ success: false, message: 'Insufficient parameter: path' });
            return;
        }

        if(req.query.path.indexOf('..') > -1) { // This string could be dangerous.
            res.status(500);
            res.json({ success: false, message: 'Forbidden' });
            return;
        }

        // Getting home dir.
        User.get(req.query.username, req.query.password, function(err, homeDir) {
            if(err) {
                res.status(500);
                res.json({ success: false, message: err.message });
                res = null;
                req = null;
                return;
            }

            let fullPath = path.join(homeDir, req.query.path);

            // Checking existence.
            fs.stat(fullPath, function(err, status) {
                if(err) {
                    if(err.code == 'ENOENT') {
                        res.status(404);
                        res.json({ success: false, message: 'File not found' });
                        
                    } else {
                        res.status(500);
                        res.json({ success: false, message: err.message });
                    }
                    
                    res = null;
                    req = null;
                    return;
                }

                if(!status.isDirectory) {
                    res.status(500);
                    res.json({ success: false, message: 'Not a directory' });
                    res = null;
                    req = null;
                    return;
                }

                // Listing files and folders.
                fs.readdir(fullPath, function(err, files) {
                    if(err) {
                        res.status(500);
                        res.json({ success: false, message: err.message });
                        res = null;
                        req = null;
                        return;
                    }

                    try {
                        // Getting details.
                        let result = [];

                        for(let i = 0; i < files.length; ++i) {
                            let item = {};
                            let status = fs.statSync(path.join(fullPath, files[i]));
                            item.name = files[i];
                            item.type = status.isDirectory() ? 'd' : '-';
                            item.size = status.size;
                            item.date = status.mtime;

                            result.push(item);
                        }

                        // Done.
                        res.status(200);
                        res.json({ success: true, result: result });

                        // Releasing memory.
                        res = null;
                        req = null;
                    } catch(ex) {
                        res.status(500);
                        res.json({ success: false, message: ex.message });
                        res = null;
                        req = null;
                    }
                });
            })
        });
    };

    // GET
    // Query: username, password, path
    module.size = function(req, res) {
        // Validating.
        if(!req.query.username) {
            res.status(500);
            res.json({ success: false, message: 'Insufficient parameter: username' });
            return;
        }
        
        if(!req.query.password) {
            res.status(500);
            res.json({ success: false, message: 'Insufficient parameter: password' });
            return;
        }

        if(!req.query.path) {
            res.status(500);
            res.json({ success: false, message: 'Insufficient parameter: path' });
            return;
        }

        if(req.query.path.indexOf('..') > -1) { // This string could be dangerous.
            res.status(500);
            res.json({ success: false, message: 'Forbidden' });
            return;
        }

        // Getting home dir.
        User.get(req.query.username, req.query.password, function(err, homeDir) {
            if(err) {
                res.status(500);
                res.json({ success: false, message: err.message });
                res = null;
                req = null;
                return;
            }

            let fullPath = path.join(homeDir, req.query.path);

            // Getting file info.
            fs.stat(fullPath, function(err, status) {
                if(err) {
                    if(err.code == 'ENOENT') {
                        res.status(404);
                        res.json({ success: false, message: 'File not found' });
                        
                    } else {
                        res.status(500);
                        res.json({ success: false, message: err.message });
                    }
                    
                    res = null;
                    req = null;
                    return;
                }

                if(!status.isFile) {
                    res.status(500);
                    res.json({ success: false, message: 'Not a file' });
                    res = null;
                    req = null;
                    return;
                }

                // Done.
                res.status(200);
                res.json({ success: true, size: status.size });

                // Releasing memory.
                res = null;
                req = null;
            });
        });
    };

    // GET
    // Query: username, password, path
    module.download = function(req, res) {
        // Validating.
        if(!req.query.username) {
            res.status(500);
            res.json({ success: false, message: 'Insufficient parameter: username' });
            return;
        }
        
        if(!req.query.password) {
            res.status(500);
            res.json({ success: false, message: 'Insufficient parameter: password' });
            return;
        }

        if(!req.query.path) {
            res.status(500);
            res.json({ success: false, message: 'Insufficient parameter: path' });
            return;
        }

        if(req.query.path.indexOf('..') > -1) { // This string could be dangerous.
            res.status(400);
            res.json({ success: false, message: 'Forbidden' });
            return;
        }

        // Getting home dir.
        User.get(req.query.username, req.query.password, function(err, homeDir) {
            if(err) {
                res.status(500);
                res.json({ success: false, message: err.message });
                res = null;
                req = null;
                return;
            }

            let fullPath = path.join(homeDir, req.query.path);

            // Getting file size.
            fs.stat(fullPath, function(err, status) {
                if(err) {
                    if(err.code == 'ENOENT') {
                        res.status(404);
                        res.json({ success: false, message: 'File not found' });
                        
                    } else {
                        res.status(500);
                        res.json({ success: false, message: err.message });
                    }
                    
                    res = null;
                    req = null;
                    return;
                }

                if(!status.isFile) {
                    res.status(500);
                    res.json({ success: false, message: 'Not a file' });
                    req = null;
                    res = null;
                    return;
                }

                // Opening file stream.
                let stream = fs.createReadStream(fullPath);

                // Writing header.
                res.writeHead(200, {
                    'Content-Type': 'application/octet-stream',
                    'Content-Length': status.size
                });

                // Piping stream.
                stream.pipe(res);

                // Done successfully.
                res.on('end', function() {
                    // Logging.
                    log('File ' + fullPath + ' is downloaded');

                    // Releasing memory.
                    req = null;
                    res = null;
                });
            });
        });
    };

    return module;
};