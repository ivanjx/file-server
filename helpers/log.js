// Logging process helper.
const DateFormat = require('dateformat');
const cluster = require('cluster');

exports.log = function(message) {
    if(cluster.isWorker) {
        // Sending log message to master process.
        // Parameter is just a string.
        cluster.worker.send({pid: process.pid, message: message});
    } else {
        // This is master process.
        // Parameter is an object.
        let dateString = DateFormat(new Date(), 'HH:MM:ss');
        console.log('[' + dateString + '][' + message.pid + '] ' + message.message);
    }
}