// User and folder linker.
// Reading list file.
const fs = require('fs');
const os = require('os');

const listFile = fs.readFileSync('list', { encoding: 'utf8' });
global.authKeys;

if(!global.authKeys) {
    global.authKeys = {};

    // Parsing.
    let list = listFile.split(os.EOL);

    for(let i = 0; i < list.length; ++i) {
        if(list[i].startsWith('#')) {
            continue;
        }
        
        let split = list[i].split(/[\s,]+/);
        let key = split[0] + split[1];
        let value = split[2];
        authKeys[key] = value;
    }
}

// Defining functions.
// callback: function(err, homeDir)
exports.get = function(username, password, callback) {
    try {
        let key = username + password;
        let value = global.authKeys[key];
        callback(null, value);
    } catch(ex) {
        callback(ex, null);
    }
}